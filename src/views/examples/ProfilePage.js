
import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import {Container} from "reactstrap";
import Item from './Item';
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import ProfilePageHeader from "components/Headers/ProfilePageHeader.js";
import DemoFooter from "components/Footers/DemoFooter.js";


class ProfilePage extends Component {
  constructor() {
    super();
    this.state = {
      description1: '', email: '', password: '',
      list: [],
      items: [],
      user: []
    };
  }
  handleChange(event) {

    this.setState({ description1: event.target.value });
  }

  addToList(input) {
    let listArray = this.state.list;
    listArray.push(input);
    this.setState({
      list: listArray
    })
    console.log(this.state.list);
  }

  add = async () => {
    const AuthStr = `Bearer ${localStorage.getItem('token')}`;
    const response = await axios.post('http://homestead.test/api/reclamation',
      { description: this.state.description1 },
      { headers: { Authorization: AuthStr } });
    console.log(response);
    this.getAll()
    this.renderItem()
  }

  componentDidMount() {

    this.getAll() 
  }
  getAll = () => {
    const AuthStr = `Bearer ${localStorage.getItem('token')}`;
    axios.get('http://homestead.test/api/reclamation', { headers: { Authorization: AuthStr } })
      .then(response => {
        this.setState({ items: response.data });
        console.log(this.state.items)
      }, (error) => {
        console.log(error);
      });
  }
  
  renderItem = () => {

    return this.state.items.map(item =>
      <Item   key={item.id} item={item}   />
    );

  }
  render() {
    return (

      <div>
        <ExamplesNavbar />
        <ProfilePageHeader />
        <div className="section profile-content">
          <Container>
            <div className="owner">
              <div className="avatar">
                <img
                  alt="..."
                  className="img-circle img-no-padding img-responsive"
                  src={require("assets/img/faces/joe-gardner-2.jpg")}
                />
              </div>
              <div className="name">
                <h4 className="title">
                  {this.state.user.nom} <br />
                </h4>
              </div>
            </div>
            <div  >
              <h1>rapport de reclamation</h1>
              <div  >
                <input
                  onChange={this.handleChange.bind(this)}
                  placeholder="mat5afech ektb" value={this.state.description1} />

                <button onClick={() => this.add()}>ajouter</button>
              </div><div >
                {this.renderItem()}
              </div></div>
          </Container>

        </div>

      </div>
    );
  }
}



export default withRouter(ProfilePage);
