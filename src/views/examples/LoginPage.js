import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom'
// reactstrap components
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";
 
class LoginPage extends Component {


  constructor() {
    super();
    this.state = {
      email: '', password: '',

    };
  }
  handleChangeEmail = (event) => {
    this.setState({ email: event.target.value });
  }
  handleChangePassword = (event) => {
    this.setState({ password: event.target.value });
  }
  componentDidMount() {
    localStorage.clear();
    console.log(localStorage);
  }
  Login = async () => {
    const response = await axios.post('http://homestead.test/api/user/login',
      { password: this.state.password, email: this.state.email });
    console.log(response);
    localStorage.setItem('token', response.data['token']);

    if (localStorage.getItem('token')) {
      this.props.history.push("/profile-page")
    } else {
    }
  }
  render() {
    return (
      <>
        <div
          className="page-header"
          style={{
            backgroundImage: "url(" + require("assets/img/login-image.jpg") + ")"
          }}
        >
          <div className="filter" />
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" lg="4">
                <Card className="card-register ml-auto mr-auto">
                  <h3 className="title mx-auto">Welcome</h3>

                  <Form className="register-form">
                    <label>Email</label>
                    <Input onChange={this.handleChangeEmail.bind(this)} placeholder="Email" type="text" />
                    <label>Password</label>
                    <Input onChange={this.handleChangePassword.bind(this)} placeholder="Password" type="password" />
                    <Button onClick={this.Login()} block className="btn-round" color="danger">
                      login
                  </Button>
                  </Form>

                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      </>
    );
  }
}


export default LoginPage;
