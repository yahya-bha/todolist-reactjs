 
import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
 

class Item extends Component {
  delete=()=> {
    const AuthStr = `Bearer ${localStorage.getItem('token')}`;
axios.delete(`http://homestead.test/api/reclamation/${this.props.item.id}`,{ headers: { Authorization: AuthStr }})
.then(res => {
  console.log(res.data);
    this.getAll() 
})
} 

 
 
  getAll = () => {
  const AuthStr = `Bearer ${localStorage.getItem('token')}`;
  axios.get('http://homestead.test/api/reclamation', { headers: { Authorization: AuthStr } })
    .then(response => {
      this.setState({ items: response.data });
      console.log(this.state.items)
    }, (error) => {
      console.log(error);
    });
}
 
 
    render(){ 
     
      return (
        <table className="ui celled definition compact table">
        <tbody className="">
          <tr  style={{display: 'flex',  justifyContent:'center', alignItems:'center', width : '50vh'}} className="">
            
            <td className="" >{this.props.item.id}</td>
            <td className="">{this.props.item.description}</td>
            
            <button onClick={this.delete} className="ui small icon primary right floated left labeled button">
                 Delete
               </button>  
           
          </tr>
        
        </tbody>
        
      </table>
    );
    }

}

export default withRouter(Item);