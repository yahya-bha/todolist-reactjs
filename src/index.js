
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

// styles
import "assets/css/bootstrap.min.css";
import "assets/scss/paper-kit.scss";
import "assets/demo/demo.css";
// pages
import Index from "views/Index.js";
import NucleoIcons from "views/NucleoIcons.js";
import LandingPage from "views/examples/LandingPage.js";
import ProfilePage from "views/examples/ProfilePage.js";
import LoginPage from "views/examples/LoginPage.js";
import Item from "views/examples/Item";
import Update from "views/examples/Update";
import PrivateRoute from '.helpers/PrivateRoute';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/index" render={props => <LoginPage {...props} />} />
      <PrivateRoute
        path="/profile-page"
        render={props => <ProfilePage {...props} />}
      />
      <PrivateRoute
        path="/item"
        render={props => <Item {...props} />}
      />
      <Route
        path="/Login-page"
        render={props => <LoginPage {...props} />}
      />
      <Redirect to="/index" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
